﻿using System.Collections.Generic;
using DryIoc;
using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Logger;
using DotStat.DB.Repository.MariaDb;
using DotStat.Db.Repository.SqlServer;
using DotStat.MappingStore;
using DotStatServices.AuthorizationManagement.Mappers;
using Mapster;
using MapsterMapper;
using Microsoft.Extensions.Configuration;

namespace DotStatServices.AuthorizationManagement
{

    [ExcludeFromCodeCoverage]
    internal class ApiIoc
    {
        public ApiIoc(IContainer container, IConfiguration configuration)
        {
            var authConfig = configuration.GetSection("auth").Get<AuthConfiguration>() ?? AuthConfiguration.Default;
            var baseConfig = configuration.Get<BaseConfiguration>();
            RegisterConfiguration(container, authConfig, baseConfig);
            RegisterServices(container, baseConfig);
        }

        private static void RegisterConfiguration(IContainer container, AuthConfiguration authConfig, BaseConfiguration baseConfig)
        {
            container.RegisterInstance(baseConfig);
            container.RegisterInstance<ILocalizationConfiguration>(baseConfig);
            container.RegisterInstance<IGeneralConfiguration>(baseConfig);
            container.RegisterInstance<IAuthConfiguration>(authConfig);

            // Log4Net
            LogHelper.ConfigureAppenders(baseConfig);
        }

        private static void RegisterServices(IContainer container, BaseConfiguration baseConfig)
        {
            var dbType = DbTypeHelper.GetDbType(baseConfig.DbType, true);
            if (dbType == DbType.MariaDb)
            {
                container.Register<IAuthorizationRepository, MariaDbAuthorizationRepository>(reuse: Reuse.Singleton);
            }
            else
            {
                container.Register<IAuthorizationRepository, SqlAuthorizationRepository>(reuse: Reuse.Singleton);
            }
            container.Register<IAuthorizationManagement, DotStat.Common.Auth.AuthorizationManagement>(Reuse.Singleton);

            // Mappers
            var config = TypeAdapterConfig.GlobalSettings;
            config.Scan(typeof(UserAuthorizationMapper).Assembly);
            container.RegisterInstance<IMapper>(new Mapper(config));
        }
    }
}