﻿using DotStat.Common.Auth;
using DotStatServices.AuthorizationManagement.Validation;
using Estat.Sdmxsource.Extension.Constant;
using FluentValidation.TestHelper;
using NUnit.Framework;

namespace Auth.Test.Unit
{
    public class UserAuthRulesValidatorTests
    {
        private UserAuthorizationValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new UserAuthorizationValidator();
        }

        [Test]
        public void Should_have_error_when_UserMask_is_null()
        {
            _validator.ShouldHaveValidationErrorFor(rule => rule.UserMask, null as string);
        }

        [Test]
        public void Should_have_error_when_Id_is_negative()
        {
            _validator.ShouldHaveValidationErrorFor(rule => rule.Id, -1);
        }

        [Test]
        public void Should_have_error_when_Permission_undefined()
        {
            _validator.ShouldHaveValidationErrorFor(rule => rule.Permission, (PermissionType) 0);
        }

        [Test]
        public void Should_not_have_error_when_Id_is_zero()
        {
            _validator.ShouldNotHaveValidationErrorFor(rule => rule.Id, 0);
        }

        [Test]
        public void Should_not_have_error_when_dataspace_is_specified()
        {
            _validator.ShouldNotHaveValidationErrorFor(rule => rule.DataSpace, "*");
        }
    }
}
