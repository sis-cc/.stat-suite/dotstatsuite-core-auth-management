﻿using DotStat.Common.Model;
using DotStatServices.AuthorizationManagement.Models;
using Mapster;

namespace DotStatServices.AuthorizationManagement.Mappers
{
    public class UserAuthorizationMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<UserAuthorization, UserAuthorizationDto>()
                .Map(d => d.ArtefactTypeString, s => s.ArtefactType);
        }
    }
}
