﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.MappingStore;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using MySqlConnector;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;


namespace DotStatServices.AuthorizationManagement.HealthCheck
{
    [ExcludeFromCodeCoverage]
    public class DbHealthCheck : IHealthCheck
    {
        private readonly IGeneralConfiguration _configuration;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public DbHealthCheck(IGeneralConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            var dbType = DbTypeHelper.GetDbType(_configuration.DbType, true);
            string databaseVersion;
            if (dbType == DbType.MariaDb) {
                databaseVersion = await CheckMariaDbConnection(_configuration.DotStatSuiteCoreCommonDbConnectionString, cancellationToken);
            }
            else {
                databaseVersion = await CheckSqlServerConnection(_configuration.DotStatSuiteCoreCommonDbConnectionString, cancellationToken);
            }

            var result = !string.IsNullOrEmpty(databaseVersion);

            var data = new Dictionary<string, object>()
            {
                {"databaseAlive", result},
                {"databaseVersion", databaseVersion},
                {"DbType", dbType.ToString()}
            };

            return result 
                ? HealthCheckResult.Healthy(data:data) 
                : HealthCheckResult.Unhealthy(data:data);
        }

        private async Task<string> CheckSqlServerConnection( string connectionString, CancellationToken cancellationToken)
        {
            try
            {
                await using var connection = new SqlConnection(connectionString);

                await connection.OpenAsync(cancellationToken);

                await using var command = connection.CreateCommand();

                command.CommandText = "select [Version] from [dbo].[DB_VERSION]";

                return await command.ExecuteScalarAsync(cancellationToken) as string;
            }
            catch
            {
                return null;
            }
        }
        
        private async Task<string> CheckMariaDbConnection( string connectionString, CancellationToken cancellationToken)
        {
            try
            {
                await using var connection = new MySqlConnection(connectionString);

                await connection.OpenAsync(cancellationToken);

                await using var command = connection.CreateCommand();

                command.CommandText = "SELECT `VERSION` FROM DB_VERSION";

                return await command.ExecuteScalarAsync(cancellationToken) as string;
            }
            catch
            {
                return null;
            }
        }
    }
}
