﻿using DryIoc;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using DotStat.Common.Configuration;
using DotStatServices.AuthorizationManagement.HealthCheck;
using DryIoc.Microsoft.DependencyInjection;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DotStatServices.AuthorizationManagement
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        private readonly AuthConfiguration _auth;

        public Startup(IConfiguration configuration)
        {
            _auth = configuration.GetSection("auth").Get<AuthConfiguration>();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            IdentityModelEventSource.ShowPII = true;

            // By default, Microsoft has some legacy claim mapping that converts standard JWT claims into proprietary ones.
            // This removes those mappings.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services
                .AddHttpContextAccessor()
                .AddMvc(options =>
                {
                    if (_auth?.Enabled == true)
                    {
                        options.Filters.Add(new AuthorizeFilter());
                    }

                    options.Filters.Add(new ProducesAttribute("application/json"));
                    options.Filters.Add(
                        new ProducesResponseTypeAttribute(typeof(OperationResult), (int) HttpStatusCode.OK));
                    options.Filters.Add(new ProducesResponseTypeAttribute(typeof(OperationResult),
                        (int) HttpStatusCode.BadRequest));
                    options.Filters.Add(new ProducesResponseTypeAttribute((int) HttpStatusCode.Unauthorized));
                    options.Filters.Add(new ProducesResponseTypeAttribute((int) HttpStatusCode.Forbidden));
                })
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                })
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly()));

            services.AddVersionedApiExplorer(o =>
            {
                o.GroupNameFormat = "'v'VVV";
                o.SubstituteApiVersionInUrl = true;
            });

            services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 1);
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true;
            });

            // -----------------------------------------------------------------------

            if (_auth?.Enabled == true)
            {
                var validAudiences = new List<string>(2)
                {
                  _auth.ClientId
                };

                if (!string.IsNullOrEmpty(_auth.Audience))
                {
                  validAudiences.Add(_auth.Audience);
                }

                services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                }).AddJwtBearer(options =>
                {
                    options.Authority = _auth.Authority;
                    options.RequireHttpsMetadata = _auth.RequireHttps;
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = _auth.ValidateIssuer,
                        ValidAudiences = validAudiences
                    };
                });
            }

            // -----------------------------------------------------------------------

            //This line adds Swagger generation services to our container.
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.1", new OpenApiInfo { Title = "DotStat User Authorizations Management", Version = "1.1" });
                //c.DescribeAllEnumsAsStrings();

                if (_auth?.Enabled == true)
                {
                    var securityScheme = new OpenApiSecurityScheme()
                    {
                        Type = SecuritySchemeType.OAuth2,
                        In = ParameterLocation.Header,
                        Flows = new OpenApiOAuthFlows()
                        {
                            AuthorizationCode = new OpenApiOAuthFlow()
                            {
                                AuthorizationUrl = new Uri(_auth.AuthorizationUrl),
                                TokenUrl = new Uri(string.IsNullOrEmpty(_auth.TokenUrl) ? _auth.AuthorizationUrl.Replace("openid-connect/auth", "openid-connect/token") : _auth.TokenUrl),
                                Scopes = _auth.Scopes.ToDictionary(x => x, x => x),
                            }
                        }
                    };

                    c.AddSecurityDefinition("oauth2", securityScheme);

                    c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                    {
                        {
                            new OpenApiSecurityScheme()
                            {
                                Reference = new OpenApiReference {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "oauth2",
                                }
                            },
                            _auth.Scopes.ToList()
                        }
                    });
                }

                c.DocInclusionPredicate((docName, apiDesc) =>
                {
                    if (!apiDesc.TryGetMethodInfo(out MethodInfo methodInfo))
                        return false;

                    var versions = methodInfo.DeclaringType
                        .GetCustomAttributes(true)
                        .OfType<ApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions);

                    return versions.Any(v => $"v{v.ToString()}" == docName);
                });

                //Locate the XML file being generated by ASP.NET...
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                //... and tell Swagger to use those XML comments.
                c.IncludeXmlComments(xmlPath);
            });

            //https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-3.1
            services.AddHealthChecks()
                .AddCheck<ServiceHealthCheck>("service", tags: new[] { "live" })
                .AddCheck<DbHealthCheck>("database")
                .AddCheck<MemoryHealthCheck>("memory");

            // Dry IOC setup
            return new Container(rules => rules.WithoutThrowIfDependencyHasShorterReuseLifespan())
                .WithDependencyInjectionAdapter(services)
                .ConfigureServiceProvider<ApiIoc>();
        }

        public void Configure(IApplicationBuilder app)
        {
            // Global exception handler
            app.UseExceptionHandler(new ExceptionHandlerOptions
            {
                ExceptionHandler = JsonExceptionMiddleware.Invoke
            });

            if (_auth?.Enabled == true)
            {
                app.UseAuthentication();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.DocumentTitle = "DotStat User Authorizations Management Web API";
                c.SwaggerEndpoint("./v1.1/swagger.json", "DotStat User Authorizations Management Web API");

                if (_auth?.Enabled == true)
                {
                    c.OAuthClientId(_auth.ClientId);
                    c.OAuthUsePkce();
                }
            });

            app.UseHealthChecks("/health", new DotStatHealthCheckOptions());
            app.UseHealthChecks("/live", new DotStatHealthCheckOptions()
            {
                Predicate = (x) => x.Tags.Contains("live")
            });

            app.UseCors(BuildCorsPolicy);
            app.UseRouting();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private void BuildCorsPolicy(CorsPolicyBuilder builder)
        {
            //todo: make configurable
            builder
                .WithOrigins("http://localhost", "http://127.0.0.1") // this is needed to add Vary header
                .SetIsOriginAllowed(o => true)
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials();
        }
    }
}