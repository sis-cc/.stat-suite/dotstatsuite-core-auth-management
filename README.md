# Auth Rules Management Service

- [Configuration](#configuration)
- [API Version 1.1](#api-version-11)

## Configuration

The configuration is loaded from *.json files as a JSON object. For a quick startup you can copy + remove `.sample`exentsion from existing template files `*.json.sample` and fill/replace default values.
It's also possible to merge JSON objects under one single node in 1 single file. The name of a config file is not important, the main thing JSON object and it's properties match description bellow.

### Example of a merged configuration

```json
{
    "DotStatSuiteCoreCommonDbConnectionString": "Data Source=localhost;Initial Catalog=DotStat.Common;User ID=USERNAME;Password=XXXXXX;TrustServerCertificate=True",
    "DbType": "SqlServer",
    "AutoLog2Google": false,
    "GoogleLogLevel": "Debug",
    "GoogleProjectId": "XXXX",
    "GoogleLogId": "AUTH_SERVICE",
    "auth": {
        "enabled":true,
        "authority": "AUTHORITY URL",
        "clientId": "VALUE",
        "requireHttps": true,
        "validateIssuer": true,
        "claimsMapping": {
            "email": "email",
            "groups": "groups"
        },
        "authorizationUrl": "AUTHORIZATION ENDPOINT URL",
        "tokenUrl": "TOKEN ENDPOINT URL",
        "scopes": [ "openid", "profile", "email" ]
    }
}
```

### Auth settings

| Setting    | Description |
|------------|-------------|
| enabled | Is openid authentication enabled
| authority | Authority url of token issuer
| clientId | Client/application Id
| requireHttps | Is HTTPS connection to OpenId authority server required
| validateIssuer | Is iss (issuer) claim in JWT token should match configured authority
| claimsMapping | Key/value mapping of a key used in the C# code to JWT token claim.
| authorizationUrl | Authorization url (used in swagger UI interface)
| tokenUrl | Token url (used in swagger UI interface), optional, if not defined will be constructed based on authorizationUrl
| scopes | Requested openId scopes (used as parameters for authorization url)

## Google logging settings (optional)
Optionally, it's possible to log activity to [Google.Gloud](https://cloud.google.com/logging/docs) with *AutoLog2Google* = true

| Setting    | Description | Default |
|------------|-------------|---------|
| AutoLog2Google | Automatically log to google cloud | false
| GoogleLogLevel | One of (DEBUG,INFO,ERROR) | DEBUG
| GoogleProjectId | Google.Cloud logging [projectId](https://cloud.google.com/dotnet/docs/reference/Google.Cloud.Logging.Log4Net/latest/configuration#projectid), required if AutoLog2Google=true | 
| GoogleLogId | Google.Cloud logging [logId](https://cloud.google.com/dotnet/docs/reference/Google.Cloud.Logging.Log4Net/latest/configuration#logid) | DOTSTAT_BACKEND

**NB!** If a service is executed otside of Google Cloud Platform it is required to set *GOOGLE_APPLICATION_CREDENTIALS* environment variable with a path to a service account JSON file for an authentication, see more [here](https://cloud.google.com/dotnet/docs/reference/Google.Cloud.Logging.Log4Net/latest/#authentication)

### Other keys

| Setting    | Description |
|------------|-------------|
| DotStatSuiteCoreCommonDbConnectionString | Connection string for the common database, used for authorization rules
| DbType | Possible values **SqlServer**, **MariaDb** (default: SqlServer)

### Token claims mapping

To support scenario when user's email address/groups set not as default claim keys in JWT token configuration contains a claims mapper. As example to extract email/groups information from JWT token with claims *email_claim* & *groups_claim* 

```json
{
  "exp": 1633000345,
  "iat": 1633000045,
  "auth_time": 1633000045,
  "jti": "3a1cdf1d-c2d3-4906-a6bb-a8a6be0b570c",
  "iss": "https://keycloak.siscc.org/auth/realms/OECD",
  "aud": [
    "app",
    "account"
  ],
  "sub": "8c722380-3fd4-4818-aa1e-b7c1d21cfce0",
  "typ": "Bearer",
  "azp": "app",
  "session_state": "a8868f31-b161-4c43-9ce0-c20e1a9ab652",
  "scope": "openid email profile groups",
  "name": "Firstname Lastname",
  "preferred_username": "dotstat-user",
  "given_name": "Firstname",
  "family_name": "Lastname",
  "email_claim": "my.email@gmail.com",
  "groups_claim": [
    "MY_GROUP1",
    "MY_GROUP2"
  ]
}
```

Following configuration should be used:

```json
{
    "auth": {
        "claimsMapping": {
            "email": "email_claim",
            "groups": "groups_claim"
        }
    }
}
```

The same configuration, but using environment variables:

```
auth__claimsMapping__email=email_claim
auth__claimsMapping__groups=groups_claim
```

## API Version 1.1

This service is API allowing to perform CRUD operations on ([dbo].[AuthorizationRules]) table, located in a Common database. This service requires CanModifyStoreSettings (64) permission from logged in user, otherwise, 403 (Forbidden) response is returned.

All api methods may return one of the following HTTP responses

- **401 (Unauthorized)** - User is not authenticated
- **406 (Forbidden)** - User doesn't have required CanModifyStoreSettings permission
- **200 (OK)** - Operation succeeded
- **400 (BadRequest)** - The was a validation or internal server error

For the authenticated user (http codes 200 & 400) response always returns a json message with the following schema:

```json
{
  "success": boolean,
  "payload": object | array | string
}
```

where

- **success** indicates the result of the operation, it's always *false* in case of error
- **payload** is the actual returned result and may vary from one method to another

### Swagger UI

- http://{sertvice-url}/swagger
- http://{sertvice-url}/swagger/v1.1/swagger.json

### Methods

- [List all authorization rules](#get-versionauthorizationrules-list-all-authorization-rules)
- [List authorization rules of an authenticated User](#get-versionauthorizationrulesme-list-authorization-rules-of-an-authenticated-user)
- [Get a rule by ID](#get-versionauthorizationrulesid-get-a-rule-by-id)
- [Delete a rule by ID](#delete-versionauthorizationrulesid-delete-a-rule-by-id)
- [Create or update a rule](#post-versionauthorizationrules-create-or-update-a-rule)
- [Check if user is authorized](#get-versionauthorizationrulesisauthorized-check-if-user-is-authorized)
- [List artefact type enumeration](#get-versiongetsdmxartefacttypes-list-artefact-type-enumeration)
- [List permission enumeration](#get-versiongetpermissiontypes-list-permission-enumeration)

#### GET /{version}/AuthorizationRules - List all authorization rules
> [Route("{version:apiVersion}/AuthorizationRules")]

**Payload** contains a collection of UserAuthorization objects.

##### Response example

```json
{
  "success": true,
  "payload": [
    {
      "id": 1,
      "userMask": "firstname.lastname@oecd.org",
      "isGroup": false,
      "dataSpace": null,
      "artefactType": 0,
      "artefactAgencyId": null,
      "artefactId": null,
      "artefactVersion": null,
      "permission": 2047
    },
    {
      "id": 2,
      "userMask": "admin",
      "isGroup": true,
      "dataSpace": "design",
      "artefactType": 0,
      "artefactAgencyId": "OECD",
      "artefactId": "*",
      "artefactVersion": "*",
      "permission": 2
    }
  ]
}
```

#### GET /{version}/AuthorizationRules/me - List authorization rules of an authenticated User
> [Route("{version:apiVersion}/AuthorizationRules/me")]

**Payload** contains a collection of UserAuthorization objects.

##### Response example

```json
{
  "success": true,
  "payload": [
    {
      "id": 1,
      "userMask": "firstname.lastname@oecd.org",
      "isGroup": false,
      "dataSpace": null,
      "artefactType": 0,
      "artefactAgencyId": null,
      "artefactId": null,
      "artefactVersion": null,
      "permission": 2047
    },
    {
      "id": 2,
      "userMask": "admin",
      "isGroup": true,
      "dataSpace": "design",
      "artefactType": 0,
      "artefactAgencyId": "OECD",
      "artefactId": "*",
      "artefactVersion": "*",
      "permission": 2
    }
  ]
}
```

#### GET /{version}/AuthorizationRules/{id} - Get a rule by ID
> [Route("{version:apiVersion}/AuthorizationRules/{id}")]

**Payload**` contains UserAuthorization object, if ID is found in the database or NULL if it's not existent.

##### Parameters
Name|Type|Description|Requirement
---|---|---|---
id|integer|ID of the rule |`Required`

##### Response example

```json
{
  "success": true,
  "payload": {
    "id": 2,
    "userMask": "admin",
    "isGroup": true,
    "dataSpace": "design",
    "artefactType": 0,
    "artefactAgencyId": "OECD",
    "artefactId": "*",
    "artefactVersion": "*",
    "permission": 2
  }
}
```

#### DELETE /{version}/AuthorizationRules/{id} - Delete a rule by ID
> [Route("/{version}/AuthorizationRules/{id}")]

**Success** returns `true` if the operation was successful and `false` if not.

#### POST /{version}/AuthorizationRules - Create or update a rule
> [Route("{version:apiVersion}/AuthorizationRules")]

Method expects UserAuthorization rule as a json object. If sent object ID property equals 0 then a new rule is created, if the ID is a positive number that corresponds to a record in the database then this record is updated with submitted data.

**Success** returns `true` if the operation was successful and `false` if not. 

##### Parameters
Name|Type|Description|Requirement
---|---|---|---
id|integer |If 0 - new object created, if > 0 then update is performed |`Required`
userMask|string |User or Group ID|`Required`
isGroup|boolean |True if rule is for group |`Required`
dataSpace|string |Dataspace that applies to a rule, * - for any |
artefactType|integer|Artefact type, one of the value from GetSDMXArtefactTypes enumeration|`Required`
artefactAgencyId|string |Agency that applies to a rule, * - for any|
artefactId|string |Artefact ID that applies to a rule, * - for any |
artefactVersion|string |Artefact Version that applies to a rule, * - for any |
permission|integer|Permission, one of the value from GetPermissionTypes enumeration|`Required`

## Request example

```json
{
    "id": 2,
    "userMask": "admin",
    "isGroup": true,
    "dataSpace": "design",
    "artefactType": 0,
    "artefactAgencyId": "OECD",
    "artefactId": "*",
    "artefactVersion": "*",
    "permission": 2
  }
```

#### GET /{version}/AuthorizationRules/IsAuthorized - Check if user is authorized
> [Route("{version:apiVersion}/AuthorizationRules/IsAuthorized")]

##### Parameters
Name|Type|Description|Requirement
---|---|---|---
dataSpace|string |Dataspace |`Required`
artefactAgencyId|string |Artefact agency |`Required`
artefactId|string |Artefact SDMX Id  |`Required`
artefactVersion|string |Artefact Version |`Required`
permission|integer|Requested permission |`Required`

##### Response example

```json
{
  "success": true,
  "payload": null
}
```

#### GET /{version}/GetSDMXArtefactTypes - List artefact type enumeration
> [Route("{version:apiVersion}/GetSDMXArtefactTypes")]

##### Response example

```json
{
  "success": true,
  "payload": [
    {
      "id": 0,
      "name": "Any"
    },
    {
      "id": 1,
      "name": "AgencyScheme"
    }
  ]
}
```

#### GET /{version}/GetPermissionTypes - List permission enumeration
> [Route("{version:apiVersion}/GetPermissionTypes")]

##### Response example
```json
{
  "success": true,
  "payload": [
    {
      "id": 0,
      "name": "None",
      "underlying": []
    },
    {
      "id": 1,
      "name": "CanReadStructuralMetadata",
      "underlying": []
    }
  ]
}
```
