# HISTORY

##  v10.2.0 (CommonDb v3.8)
### Description

### Issues
- [#62](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/62) Added support for MariaDB
- [#455](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/455) Update nuget references to match with ESTAT v8.19.1
- [#485](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/485) Update nuget references to match with ESTAT v8.19.5
- [#492](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/492) Update nuget references to match with ESTAT v8.19.6


##  v10.1.1 (CommonDb v3.8)
### Description

### Issues
- [#67](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/67) Addressed recent vulnerabilities
- [#85](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-docker-compose/-/issues/85) Updated the docker image description to avoid duplication of configuration documentation
- [#392](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/392) Updated nuget references to match with ESTAT v8.18.4
- [#402](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/402) Updated nuget references to match with ESTAT v8.18.6
- [#410](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/410) Updated nuget references to match with ESTAT v8.18.7
- [#426](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/426) Update nuget references to match with ESTAT v8.18.9
- [#451](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/451) Update nuget references to match with ESTAT v8.19.0
- Updated license information


##  v10.1.0 (CommonDb v3.8)
### Description

### Issues
- [#22](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/22) Changed permission requirement from AdminRole to CanModifyStoreSettings for admin related features
- [#53](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/53) Fixed vulnerabilities of siscc/dotstatsuite-core-auth-management image revealed by docker scout
- [#374](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/374) Updated nuget references to match with ESTAT v8.18.0
- [#377](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/377) Updated nuget references to match with ESTAT v8.18.2


##  v10.0.0 (CommonDb v3.8)
### Description

### Issues
- [#51](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/51) Base docker image changed from debian to alpine; Upgrade of google log4net lib as the old one was failing in alpine linux
- [#313](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/313) Updated nuget references to match with ESTAT v8.13.0
- [#325](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/325) Updated nuget references to match with ESTAT v8.15.0
- [#331](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/331) Updated nuget references to match with ESTAT v8.15.1
- [#355](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/355) Updated nuget references to match with ESTAT v8.16.0
- [#361](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/361) Updated nuget references to match with ESTAT v8.17.0
- [#563](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/563) Private repository replaced to Gitlab
- Updated license information


##  v9.1.0 (CommonDb v3.8)
### Description

### Issues
- [#289](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/289) References updated to match with NSI WS v8.12.2
- [#426](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/426) Updated authentication to support ADFS
- [#478](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/478) Updated GCP auth. in devops environments for .NET services


##  v9.0.1 (CommonDb v3.8)
### Description

### Issues
- Disabled resource auto detection of Google logger


##  v9.0.0 (CommonDb v3.8)
### Description

### Issues
- [#5](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/5) Added gitlab dast scan
- [#29](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/29) Added nuget dependency scanning to CI pipeline
- [#47](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/47) Upgraded from .NET Core 3.1 to .NET 6
- [#267](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/267) Added log4net google appender


##  v6.0.0 (CommonDb v3.8)
### Description

### Issues
- [#234](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/234) References updated to NSI v8.9.2 
- [#224](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/224) References updated to match Eurostat NSI v8.9.1 


##  v5.5.0 (CommonDb v3.8)
### Description

### Issues
- [#187](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/187) References updated to match Eurostat NSI v8.8.0 

##  v5.4.1 (CommonDb v3.8)
### Description

### Issues
- [#39](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/39) Readme appended with description of changing authentication token claims mapping
- [#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/174) References updated to match Eurostat NSI v8.7.1


##  v5.4.0 (CommonDb v3.8)
### Description

### Issues
- [#33](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/33) Added TokenUrl param to auth config
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/161) References updated to match Eurostat NSI v8.5.0


##  v5.3.0 (CommonDb v3.8)
### Description

### Issues
[#72](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/72) Added support for creation of read-only user to DbUp
[#116](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/116) Added support for Azure SQL Database and Azure SQL Managed Instance to DbUp


##  v5.2.0 (CommonDb v3.7)
### Description

### Issues
[#29](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/29) Added MyRules to list authenticated user rules
[#31](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/31) Added cors, without configuration


##  v5.1.0 (CommonDb v3.6)
### Description

### Issues
[#20](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/25) PermissionType.None (0) removed and related validation changed. 


##  v5.0.0 (CommonDb v3.5)
### Description

==Important== Before using this software, be sure to have backed up databases.
This version has major update with breaking changes to the Common database. 

This release contains upgrades of the Eurostat nuget package(s), the changelog for this can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/maapi.net.mirrored/-/blob/master/CHANGELOG.md)

### Issues
- [#103](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/103) References updated to match Eurostat NSI v8.1.2.
- [#108](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/108) Use Estat PermissionType .
- [#7](https://gitlab.com/sis-cc/.stat-suite/keycloak/-/issues/7) change from implicit flow => Authorization code flow.
- [#20](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/20) Update Admin permissions in auth DB.


##  v4.0.0 (CommonDb v2.1)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/93
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/12

##  v3.1.1 2020-04-19 (Uses CommonDb v2.1)

- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/13

##  v3.0.1 2020-01-29 (Uses CommonDb v2.1)
This release contains breaking changes with changes to the authentication management.

- /sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/66
- /sis-cc/.stat-suite/dotstatsuite-core-auth-management/issues/7