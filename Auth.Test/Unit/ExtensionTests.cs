﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotStat.Common.Auth;
using DotStat.Common.Model;
using DotStatServices.AuthorizationManagement;
using Estat.Sdmxsource.Extension.Constant;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using NUnit.Framework;

namespace Auth.Test.Unit
{
    [TestFixture]
    public class ExtensionTests
    {
        private IList<UserAuthorization> _allRules;
        private IList<UserInfo> _allUsers;

        [Test]
        public void Get_Model_Errors()
        {
            var modelState = new ModelStateDictionary();

            modelState.AddModelError("field-1", "Error message 1-1");
            modelState.AddModelError("field-1", "Error message 1-1");
            modelState.AddModelError("field-2", "Error message 2-1");
            modelState.AddModelError("field-2", "Error message 2-2");

            var errors = modelState.GetErrors();

            Assert.IsNotNull(errors);
            Assert.AreEqual(4, errors.Count());
        }

        [Test]
        public void TestScope()
        {
            InitScopeTest();

            foreach (var user in _allUsers)
            {
                var claims = new List<Claim>()
                {
                    new Claim("email", user.Email),
                };

                foreach (var group in user.GroupList)
                {
                    claims.Add(new Claim("groups", group));
                }

                var userPrincipal = new DotStatPrincipal(new ClaimsPrincipal(new ClaimsIdentity(claims)), null);

                var callerRules = _allRules.Where(r =>
                    (!r.IsGroup && (r.UserMask.Equals("*") ||
                                    r.UserMask.Equals(user.UserId, StringComparison.InvariantCultureIgnoreCase))) ||
                    (r.IsGroup && (user.GroupList.Contains(r.UserMask, StringComparer.InvariantCultureIgnoreCase)))).ToArray();

                foreach (var rule in _allRules)
                {
                    var isInScope = rule.InScope(callerRules, userPrincipal);

                    var isInExpectedScope = user.RulesInUserScope.Contains((AuthPermissionType)rule.Id);

                    Assert.AreEqual(isInExpectedScope, isInScope,
                        $"Rule {((AuthPermissionType) rule.Id).ToString()} is {(isInExpectedScope ? "" : "not ")}expected to be in scope of user {user.UserId} but it is {(isInScope ? "" : "not ")}there.");
                }
            }
        }

        private void InitScopeTest()
        {
            _allRules = new List<UserAuthorization>()
            {
                new UserAuthorization() // Full CanModifyStoreSettings #1
                {
                    Id = (int) AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedGroupPermission, UserMask = "fa1@auth.test", IsGroup = false, DataSpace = "*",
                    Permission = PermissionType.CanModifyStoreSettings
                },
                new UserAuthorization() // Full CanModifyStoreSettings #2
                {
                    Id = (int) AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedUserPermission, UserMask = "full-can-modify-store-settings-group", IsGroup = true, DataSpace = "*",
                    Permission = PermissionType.CanModifyStoreSettings
                },
                new UserAuthorization() // Reset dataspace CanModifyStoreSettings #1
                {
                    Id = (int) AuthPermissionType.CanModifyStoreSettingsOnResetDataspaceDedicatedUserPermission, UserMask = "ra1@auth.test", IsGroup = false, DataSpace = "reset",
                    Permission = PermissionType.CanModifyStoreSettings
                },
                new UserAuthorization() // Reset dataspace CanModifyStoreSettings #2
                {
                    Id = (int) AuthPermissionType.CanModifyStoreSettingsOnResetDataspaceDedicatedGroupPermission, UserMask = "reset-can-modify-store-settings-group", IsGroup = true, DataSpace = "reset",
                    Permission = PermissionType.CanModifyStoreSettings
                },
                new UserAuthorization() // Stable dataspace CanModifyStoreSettings #1
                {
                    Id = (int) AuthPermissionType.CanModifyStoreSettingsOnStableDataspaceDedicatedUserPermission, UserMask = "sa1@auth.test", IsGroup = false, DataSpace = "stable",
                    Permission = PermissionType.CanModifyStoreSettings
                },
                new UserAuthorization() // Stable dataspace CanModifyStoreSettings #2
                {
                    Id = (int) AuthPermissionType.CanModifyStoreSettingsOnStableDataspaceDedicatedGroupPermission, UserMask = "stable-can-modify-store-settings-group", IsGroup = true, DataSpace = "stable",
                    Permission = PermissionType.CanModifyStoreSettings
                },
                new UserAuthorization() // Full user #1
                {
                    Id = (int) AuthPermissionType.UserOnAllDataspacesDedicatedUserPermission, UserMask = "fu1@auth.test", IsGroup = false, DataSpace = "*",
                    Permission = PermissionType.WsUserRole
                },
                new UserAuthorization() // Full user #2
                {
                    Id = (int) AuthPermissionType.UserOnAllDataspacesDedicatedGroupPermission, UserMask = "full-user-group", IsGroup = true, DataSpace = "*",
                    Permission = PermissionType.WsUserRole
                },
                new UserAuthorization() // Reset dataspace user #1
                {
                    Id = (int) AuthPermissionType.UserOnResetDataspaceDedicatedUserPermission, UserMask = "ru1@auth.test", IsGroup = false, DataSpace = "reset",
                    Permission = PermissionType.WsUserRole
                },
                new UserAuthorization()  // Reset dataspace user #2
                {
                    Id = (int) AuthPermissionType.UserOnResetDataspaceDedicatedGroupPermission, UserMask = "reset-user-group", IsGroup = true, DataSpace = "reset",
                    Permission = PermissionType.WsUserRole
                },
                new UserAuthorization() // Stable dataspace user #1
                {
                    Id = (int) AuthPermissionType.UserOnStableDataspaceDedicatedUserPermission, UserMask = "su1@auth.test", IsGroup = false, DataSpace = "stable",
                    Permission = PermissionType.WsUserRole
                },
                new UserAuthorization() // Stable dataspace user #2
                {
                    Id = (int) AuthPermissionType.UserOnStableDataspaceDedicatedGroupPermission, UserMask = "stable-user-group", IsGroup = true, DataSpace = "stable",
                    Permission = PermissionType.WsUserRole
                },
                new UserAuthorization() // All users on all dataspacees general permission
                {
                    Id = (int) AuthPermissionType.UserOnAllDataspacesGeneralUserPermission, UserMask = "*", IsGroup = false, DataSpace = "*",
                    Permission = PermissionType.CanReadStructuralMetadata
                },
                new UserAuthorization() // All users on reset dataspace permission
                {
                    Id = (int) AuthPermissionType.UserOnResetDataspaceGeneralUserPermission, UserMask = "*", IsGroup = false, DataSpace = "reset",
                    Permission = PermissionType.WsUserRole
                },
                new UserAuthorization() // All users on stable dataspace permission
                {
                    Id = (int) AuthPermissionType.UserOnStableDataspaceGeneralUserPermission, UserMask = "*", IsGroup = false, DataSpace = "stable",
                    Permission = PermissionType.DomainUserRole
                }
            };

            _allUsers = new List<UserInfo>()
            {
                new UserInfo()
                {
                    UserId = "fa1@auth.test", Email = "fa1@auth.test", Groups = "",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnResetDataspaceDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnResetDataspaceDedicatedGroupPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnStableDataspaceDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnStableDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.UserOnResetDataspaceDedicatedUserPermission,
                        AuthPermissionType.UserOnResetDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnStableDataspaceDedicatedUserPermission,
                        AuthPermissionType.UserOnStableDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                },
                new UserInfo()
                {
                    UserId = "fa2@auth.test", Email = "fa2@auth.test", Groups = "full-can-modify-store-settings-group",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnResetDataspaceDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnResetDataspaceDedicatedGroupPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnStableDataspaceDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnStableDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.UserOnResetDataspaceDedicatedUserPermission,
                        AuthPermissionType.UserOnResetDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnStableDataspaceDedicatedUserPermission,
                        AuthPermissionType.UserOnStableDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                },
                new UserInfo()
                {
                    UserId = "ra1@auth.test", Email = "ra1@auth.test", Groups = "",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnResetDataspaceDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnResetDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.UserOnResetDataspaceDedicatedUserPermission,
                        AuthPermissionType.UserOnResetDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                },
                new UserInfo()
                {
                    UserId = "ra2@auth.test", Email = "ra2@auth.test", Groups = "reset-can-modify-store-settings-group",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnResetDataspaceDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnResetDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.UserOnResetDataspaceDedicatedUserPermission,
                        AuthPermissionType.UserOnResetDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                },
                new UserInfo()
                {
                    UserId = "sa1@auth.test", Email = "sa1@auth.test", Groups = "",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnStableDataspaceDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnStableDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.UserOnStableDataspaceDedicatedUserPermission,
                        AuthPermissionType.UserOnStableDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                },
                new UserInfo()
                {
                    UserId = "sa2@auth.test", Email = "sa2@auth.test", Groups = "stable-can-modify-store-settings-group",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnStableDataspaceDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnStableDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.UserOnStableDataspaceDedicatedUserPermission,
                        AuthPermissionType.UserOnStableDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                },
                new UserInfo()
                {
                    UserId = "fu1@auth.test", Email = "fu1@auth.test", Groups = "",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.UserOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                },
                new UserInfo()
                {
                    UserId = "fu2@auth.test", Email = "fu2@auth.test", Groups = "full-user-group",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.UserOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                },
                new UserInfo()
                {
                    UserId = "ru1@auth.test", Email = "ru1@auth.test", Groups = "",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.UserOnResetDataspaceDedicatedUserPermission,
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                },
                new UserInfo()
                {
                    UserId = "ru2@auth.test", Email = "ru2@auth.test", Groups = "reset-user-group",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.UserOnResetDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                },
                new UserInfo()
                {
                    UserId = "su1@auth.test", Email = "su1@auth.test", Groups = "",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.UserOnStableDataspaceDedicatedUserPermission,
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                },
                new UserInfo()
                {
                    UserId = "su2@auth.test", Email = "su2@auth.test", Groups = "stable-user-group",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.UserOnStableDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                },
                new UserInfo()
                {
                    UserId = "rasu2@auth.test", Email = "rasu2@auth.test",
                    Groups = "reset-can-modify-store-settings-group,stable-user-group",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnResetDataspaceDedicatedUserPermission,
                        AuthPermissionType.CanModifyStoreSettingsOnResetDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedUserPermission,
                        AuthPermissionType.UserOnAllDataspacesDedicatedGroupPermission,
                        AuthPermissionType.UserOnResetDataspaceDedicatedUserPermission,
                        AuthPermissionType.UserOnResetDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnStableDataspaceDedicatedGroupPermission,
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                },
                new UserInfo()
                {
                    UserId = "nu1@auth.test", Email = "nu1@auth.test", Groups = "",
                    RulesInUserScope = new[]
                    {
                        AuthPermissionType.UserOnAllDataspacesGeneralUserPermission,
                        AuthPermissionType.UserOnResetDataspaceGeneralUserPermission,
                        AuthPermissionType.UserOnStableDataspaceGeneralUserPermission
                    }
                }
            };
        }

        internal class UserInfo
        {
            public string UserId;
            public string Email;
            public string Groups;
            public AuthPermissionType[] RulesInUserScope;

            public IList<string> GroupList => Groups.Split(',');
        }

        internal enum AuthPermissionType
        {
            CanModifyStoreSettingsOnAllDataspacesDedicatedUserPermission = 1,
            CanModifyStoreSettingsOnAllDataspacesDedicatedGroupPermission = 2,
            CanModifyStoreSettingsOnResetDataspaceDedicatedUserPermission = 3,
            CanModifyStoreSettingsOnResetDataspaceDedicatedGroupPermission = 4,
            CanModifyStoreSettingsOnStableDataspaceDedicatedUserPermission = 5,
            CanModifyStoreSettingsOnStableDataspaceDedicatedGroupPermission = 6,

            UserOnAllDataspacesDedicatedUserPermission = 7,
            UserOnAllDataspacesDedicatedGroupPermission = 8,
            UserOnResetDataspaceDedicatedUserPermission = 9,
            UserOnResetDataspaceDedicatedGroupPermission = 10,
            UserOnStableDataspaceDedicatedUserPermission = 11,
            UserOnStableDataspaceDedicatedGroupPermission = 12,

            UserOnAllDataspacesGeneralUserPermission = 13,
            UserOnResetDataspaceGeneralUserPermission = 14,
            UserOnStableDataspaceGeneralUserPermission = 15
        }
    }
}
