﻿namespace DotStatServices.AuthorizationManagement.Models
{
    public record UserAuthorizationDto
    {
        public int Id { get; set; }

        public string UserMask { get; set; }

        public bool IsGroup { get; set; }

        public string DataSpace { get; set; }

        public int ArtefactType { get; set; }
        public string ArtefactTypeString { get; set; }

        public string ArtefactAgencyId { get; set; }

        public string ArtefactId { get; set; }

        public string ArtefactVersion { get; set; }

        public int Permission { get; set; }
    }
}
