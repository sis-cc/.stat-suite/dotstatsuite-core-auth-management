﻿using DotStat.Common.Model;
using FluentValidation;

namespace DotStatServices.AuthorizationManagement.Validation
{
    /// <summary>
    /// 
    /// </summary>
    public class UserAuthorizationValidator : AbstractValidator<UserAuthorization>
    {
        /// <summary>
        /// 
        /// </summary>
        public UserAuthorizationValidator()
        {
            RuleFor(x => x.Id).GreaterThanOrEqualTo(0);
            RuleFor(x => x.UserMask).NotEmpty();
            RuleFor(x => (int)x.Permission).GreaterThanOrEqualTo(1);

            RuleFor(x => x.DataSpace).NotEmpty();
            RuleFor(x => x.ArtefactAgencyId).NotEmpty();
            RuleFor(x => x.ArtefactId).NotEmpty();
            RuleFor(x => x.ArtefactVersion).NotEmpty();
        }
    }
}
