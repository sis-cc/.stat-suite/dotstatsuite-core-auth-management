﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace DotStatServices.AuthorizationManagement
{
    [ExcludeFromCodeCoverage]
    public class OperationResult<T> : OperationResult where T:class
    {
        public new T Payload => (T) base.Payload;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="success"></param>
        /// <param name="payload"></param>
        public OperationResult(bool success = true, T payload = null) : base(success, payload)
        {}
    }

    [ExcludeFromCodeCoverage]
    public class OperationResult
    {
        public static OperationResult Error(IEnumerable<string> errors)
        {
            return new OperationResult<ErrorsSummary>(false, new ErrorsSummary(errors));
        }

        public static OperationResult Error(string error, string detail = null)
        {
            return new OperationResult<ErrorsSummary>(false, new ErrorsSummary(new []{error}, detail));
        }

        public bool Success { get; }
        public object Payload { get; set; }

        public OperationResult(bool success = true, object payload = null)
        {
            Success = success;
            Payload = payload;
        }

        public class ErrorsSummary
        {
            public IEnumerable<string> Errors { get; set; }
            public string Detail { get; set; }

            public ErrorsSummary(IEnumerable<string> errors, string detail = null)
            {
                Errors = errors;
                Detail = detail;
            }
        }
    }
}