FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine AS build
WORKDIR /app

ARG NUGET_FEED=https://api.nuget.org/v3/index.json

# copy only needed files to restore nuget packages
COPY DotStatServices.AuthorizationManagement/*.csproj ./DotStatServices.AuthorizationManagement/
COPY nuget.config global.json version.json Directory.Build.props ./
WORKDIR /app/DotStatServices.AuthorizationManagement

# restore nuget packages
RUN dotnet restore -r linux-musl-x64

# copy everything else
COPY . /app

# publish
RUN dotnet publish -c Release --no-restore -o /out -r linux-musl-x64

FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine AS runtime
WORKDIR /app
COPY --from=build /out .

#install extra libs (icu-libs) needed in alpine based docker image 
RUN apk update && apk add --no-cache icu-libs
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false

ENTRYPOINT ["dotnet", "DotStatServices.AuthorizationManagement.dll"]