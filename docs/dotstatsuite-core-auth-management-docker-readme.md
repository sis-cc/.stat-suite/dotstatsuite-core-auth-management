# About this Image

The official authorization management service image of SIS-CC .Stat Suite, configured to be ready for using in a [.Stat Suite](https://sis-cc.gitlab.io/dotstatsuite-documentation/) installation.

Source repository: https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management

# Environment Variables
You can use environment variables to configure the dotstatsuite-core-auth-management service.
For a complete list of supported environment variables, please see the configuration section of the documentation: https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management#configuration

# How to use this image
Start dotstatsuite-core-auth-management instance using the latest release. 

IMPORTANT NOTE: If you are using PowerShell on Windows to run these commands use double quotes instead of single quotes.

Example:
```sh
docker run -e 'auth__enabled=true' \
-e 'auth__authority=http://keycloak:8080/auth/realms/${KEYCLOAK_REALM}' \
-e 'auth__clientId=yourClientID' \
-e 'auth__authorizationUrl=http://${KEYCLOAK_HOST}:${KEYCLOAK_PORT}/auth/realms/${KEYCLOAK_REALM}/protocol/openid-connect/auth' \
-e 'auth__scopes__0=openid' \
-e 'auth__scopes__1=profile' \
-e 'auth__scopes__2=email' \
-e 'auth__claimsMapping__email=email' \
-e 'auth__claimsMapping__groups=groups' \
-e 'DotStatSuiteCoreCommonDbConnectionString=Server=db;Database=CommonDb;User=testLoginCommon;Password=testLogin(!)Password;' \
-e 'DbType=SqlServer' \
-p 94:80 \
siscc/dotstatsuite-core-auth-management:master
```
