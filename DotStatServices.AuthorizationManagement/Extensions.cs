﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Auth;
using DotStat.Common.Model;
using Estat.Sdmxsource.Extension.Constant;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace DotStatServices.AuthorizationManagement
{
    public static class Extensions
    {
        public static IEnumerable<string> GetErrors(this ModelStateDictionary modelState)
        {
            return modelState.Values.SelectMany(x => x.Errors.Select(e => e.ErrorMessage));
        }

        /// <summary>
        /// Checks if the rule is in scope of the user. The user shall see the rule if 
        /// - the rule is assigned to the user or any of his/her groups
        /// - the rule corresponds to a dataspace the user CanModifyStoreSettings of (has CanModifyStoreSettings permission on that dataspace via direct grant or group permission).
        /// </summary>
        /// <param name="rule">The rule to be checked whether is in scope of rules of the principal</param>
        /// <param name="scopeRules">Authorization rules granted to current principal</param>
        /// <param name="principal">Current principal</param>
        /// <returns></returns>
        public static bool InScope(this UserAuthorization rule,  IEnumerable<UserAuthorization> scopeRules, DotStatPrincipal principal)
        {
            if (rule == null || scopeRules == null)
                return false;

            var canModifyStoreSettingsRules = scopeRules.Where(r => r.Permission.HasFlag(PermissionType.CanModifyStoreSettings)).ToArray();

            return canModifyStoreSettingsRules.Any() && ( 
                       rule.DataSpace.Equals("*") || // Current user has CanModifyStoreSettings permission on at least 1 dataspace and rule applies to all dataspaces
                       canModifyStoreSettingsRules.Any(r =>
                           r.DataSpace.Equals("*") || // Current user has CanModifyStoreSettings permission on all dataspaces
                           r.DataSpace.Equals(rule.DataSpace, StringComparison.InvariantCultureIgnoreCase))) || // Current user has CanModifyStoreSettings permission on the dataspace of the rule
                   !rule.IsGroup && (rule.UserMask.Equals("*") || // Rule applies to all users
                                     rule.UserMask.Equals(principal.UserId, StringComparison.InvariantCultureIgnoreCase)) // Rule applies to current user
                   || rule.IsGroup && principal.Groups.Contains(rule.UserMask, StringComparer.InvariantCultureIgnoreCase); // Rule applies to group of current user

        }
    }
}
