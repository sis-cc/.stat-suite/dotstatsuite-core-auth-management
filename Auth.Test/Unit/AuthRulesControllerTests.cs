﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Model;
using DotStatServices.AuthorizationManagement;
using DotStatServices.AuthorizationManagement.Controllers;
using Estat.Sdmxsource.Extension.Constant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace Auth.Test.Unit
{
    [TestFixture]
    public class AuthRulesControllerTests
    {
        private string _email;
        private IEnumerable<string> _groups;
        private IList<UserAuthorization> _rules;

        private IHttpContextAccessor _contextAccessor;
        private IAuthorizationManagement _mgmt;
        private IAuthConfiguration _configuration;

        private Mock<IAuthorizationRepository> repoMock = new Mock<IAuthorizationRepository>();
        private AuthorizationRulesController _controller;

        public AuthRulesControllerTests()
        {
            // -------------------------------------

            _rules = new List<UserAuthorization>()
            {
                new UserAuthorization(1, "design", "OECD", "DF1", "2.0", PermissionType.DataImporterRole) {UserMask = "*"},
                new UserAuthorization(2, "process", "ISTAT", "*", "*", PermissionType.AdminRole) {UserMask = "*"},
                new UserAuthorization(3, "disseminate", "*", "*", "*", PermissionType.CanReadData) {UserMask = "*"},
                new UserAuthorization(4, "*", "*", "*", "*", PermissionType.CanImportData) {UserMask = "*"}
            };

            repoMock
                .Setup(x => x.GetAll())
                .Returns(_rules);

            repoMock
                .Setup(x => x.GetByUser(It.IsAny<DotStatPrincipal>()))
                .Returns(_rules);

            repoMock
                .Setup(x => x.GetById(It.IsAny<int>()))
                .Returns<int>((id)=>_rules.FirstOrDefault(x=>x.Id.Equals(id)));

            repoMock
                .Setup(x => x.Delete(It.Is<int>(id => _rules.FirstOrDefault(r=>r.Id == id) !=null )))
                .Returns(1);

            repoMock
                .Setup(x => x.Insert(It.IsAny<UserAuthorization>()))
                .Returns(1);

            repoMock
                .Setup(x => x.Update(It.IsAny<UserAuthorization>()))
                .Returns(1);

            _mgmt = new AuthorizationManagement(repoMock.Object);

            // -----------------------------------

            _email = "mail@host.com";
            _groups = new[] { "GROUP1", "GROUP2", "GROUP3" };

            var claims = new List<Claim>()
            {
                new Claim("claim/email", _email),
            };

            foreach (var g in _groups)
                claims.Add(new Claim("claim/groups", g));

            var contextMock = new Mock<IHttpContextAccessor>();

            contextMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity(claims)));

            _contextAccessor = contextMock.Object;

            // ----------------------------------------

            _configuration = new AuthConfiguration()
            {
                Enabled = true,
                ClaimsMapping = new Dictionary<string, string>()
                {
                    {"email", "claim/email"},
                    {"groups", "claim/groups"}
                }
            };

            // -----------------------------------------------

            _controller = new AuthorizationRulesController(_contextAccessor, _mgmt, _configuration, null);
        }

        [TearDown]
        public void TearDown()
        {
            _controller.ModelState.Clear();
            repoMock.Invocations.Clear();
        }

        [Test]
        public void Get_All()
        {
            var result = _controller.GetAll();

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);

            Assert.IsNotNull(result.Value.Payload);
            Assert.AreEqual(_rules.Count, result.Value.Payload.Count());
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(10)]
        public void Get_By_Id(int id)
        {
            var expectedRule = _rules.FirstOrDefault(x => x.Id.Equals(id));

            var result = _controller.GetById(id);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);

            Assert.AreEqual(expectedRule!=null, result.Value.Success);
            Assert.AreEqual(expectedRule, result.Value.Payload);
        }

        [TestCase("design", "OECD", "DF0", "1.0", PermissionType.CanReadData, false)]
        [TestCase("design", "OECD", "DF1", "2.0", PermissionType.CanReadData, true)]
        [TestCase("process", "ISTAT", "DF2", "3.0", PermissionType.AdminRole, true)]
        [TestCase("process", "ISTAT", "DF3", "4.0", PermissionType.CanReadData, true)]
        [TestCase("process", "SOME", "DF3", "4.0", PermissionType.CanReadData, false)]
        [TestCase("none", "ISTAT", "DF2", "3.0", PermissionType.CanReadData, false)]
        [TestCase("disseminate", "ISTAT", "DF2", "3.0", PermissionType.CanReadData, true)]
        [TestCase("disseminate", "ISTAT", "DF3", "4.0", PermissionType.CanReadData, true)]
        [TestCase("disseminate", "XXX", "DF3", "4.0", PermissionType.CanReadData, true)]
        [TestCase("disseminate", "ISTAT", "DF3", "4.0", PermissionType.CanImportData, true)]
        [TestCase("new", "ZZZ", "DF5", "10.0", PermissionType.CanImportData, true)]
        [TestCase("new", "ZZZ", "DF5", "10.0", PermissionType.CanReadData, false)]
        public void IsAuthorized(
            string dataSpace, 
            string artefactAgencyId, 
            string artefactId, 
            string artefactVersion, 
            PermissionType requestedPermission,
            bool expectedResult
        )
        {
            var result = _controller.IsAuthorized(dataSpace, artefactAgencyId, artefactId, artefactVersion, requestedPermission);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);

            Assert.AreEqual(expectedResult, result.Value.Success);
            Assert.IsNull(result.Value.Payload);
        }

        [Test]
        public void Post_With_Non_Existing_Id()
        {
            var rule = new UserAuthorization() { Id = 10, DataSpace = "process", ArtefactAgencyId = "ISTAT"};

            var result = _controller.Post(rule);

            repoMock.Verify(mock => mock.GetById(rule.Id), Times.Once);
            repoMock.Verify(mock => mock.Update(rule), Times.Never);

            Assert.IsNotNull(result);

            var badRequest = result.Result as BadRequestObjectResult;

            Assert.IsNotNull(badRequest);

            var operationResult = badRequest.Value as OperationResult<OperationResult.ErrorsSummary>;

            Assert.IsNotNull(operationResult);
            Assert.AreEqual(false, operationResult.Success);
            Assert.IsNotNull(operationResult.Payload);
            Assert.AreEqual($"There is no rule with ID: {rule.Id}", operationResult.Payload.Errors.First());
        }

        [Test]
        public void Post_Rule_4_Update()
        {
            var rule = new UserAuthorization()
            {
                Id = 2,
                DataSpace = "process",
                ArtefactAgencyId = "ISTAT",
                ArtefactId = "dummy",
                ArtefactVersion = "1.0"
            };

            var result = _controller.Post(rule);

            repoMock.Verify(mock => mock.GetById(rule.Id), Times.Once);
            repoMock.Verify(mock => mock.Update(rule), Times.Once);

            Assert.IsNotNull(rule.EditedBy);
            Assert.AreEqual(DateTime.Today, rule.EditDate.Date);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);

            Assert.IsTrue(result.Value.Success);
            Assert.IsNull(result.Value.Payload);
        }

        [Test]
        public void Post_Rule_4_Insert()
        {
            var rule = new UserAuthorization()
            {
                Id = 0,
                UserMask = "test",
                DataSpace = "process",
                ArtefactAgencyId = "ISTAT",
                ArtefactId = "dummy",
                ArtefactVersion = "1.0"
            };

            var result = _controller.Post(rule);

            repoMock.Verify(mock => mock.Insert(rule), Times.Once);

            Assert.IsNotNull(rule.EditedBy);
            Assert.AreEqual(DateTime.Today, rule.EditDate.Date);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);

            Assert.IsTrue(result.Value.Success);
            Assert.IsNull(result.Value.Payload);
        }

        [Test]
        public void Post_Invalid_Rule()
        {
            _controller.ModelState.AddModelError("dummy", "Error");
            var result = _controller.Post(new UserAuthorization());

            Assert.IsNotNull(result);

            var badRequest = result.Result as BadRequestObjectResult;

            Assert.IsNotNull(badRequest);

            var operationResult = badRequest.Value as OperationResult<OperationResult.ErrorsSummary>;

            Assert.IsNotNull(operationResult);
            Assert.AreEqual(false, operationResult.Success);
            Assert.IsNotNull(operationResult.Payload);
            Assert.AreEqual(1, operationResult.Payload.Errors.Count());
        }

        [TestCase(1, false)]
        [TestCase(2, true)]
        [TestCase(10, false)]
        [TestCase(-1, false)]
        public void Delete(int id, bool expectedResult)
        {
            var result = _controller.Delete(id);

            Assert.IsNotNull(result);
            Assert.AreEqual(expectedResult, result.Value?.Success ?? false);
        }

        [Test]
        public void Get_Artefact_Types()
        {
            var result = _controller.GetArtefactTypes();

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);

            var payload = result.Value.Payload as IEnumerable<object>;

            Assert.IsNotNull(payload);
            Assert.IsTrue(payload.Any());
        }

        [Test]
        public void Get_Permission_Types()
        {
            var result = _controller.GetPermissionTypes();

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);

            var payload = result.Value.Payload as IEnumerable<object>;

            Assert.IsNotNull(payload);
            Assert.IsTrue(payload.Any());
        }

    }
}
