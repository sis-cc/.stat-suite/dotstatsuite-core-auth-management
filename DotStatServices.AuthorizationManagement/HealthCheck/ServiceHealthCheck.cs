﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace DotStatServices.AuthorizationManagement.HealthCheck
{
    [ExcludeFromCodeCoverage]
    public class ServiceHealthCheck : IHealthCheck
    {
        private readonly BaseConfiguration _configuration;
        private readonly IAuthConfiguration _authConfiguration;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="authConfiguration"></param>
        public ServiceHealthCheck(BaseConfiguration configuration, IAuthConfiguration authConfiguration)
        {
            _configuration = configuration;
            _authConfiguration = authConfiguration;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            return _configuration != null && _authConfiguration!=null
                ? HealthCheckResult.Healthy(data: WebInfo())
                : HealthCheckResult.Unhealthy();
        }

        private Dictionary<string, object> WebInfo()
        {
            return new Dictionary<string, object>()
            {
                {"version", GetInformationalVersion(Assembly.GetExecutingAssembly())},
                {"auth_enabled", _authConfiguration.Enabled}
            };
        }

        private string GetInformationalVersion(Assembly assembly)
        {
            return FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion;
        }
    }
}
